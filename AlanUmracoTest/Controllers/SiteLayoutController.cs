﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Core.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;
//using System.Runtime.Caching;

namespace AlanUmracoTest.Controllers
{
    public class SiteLayoutController : SurfaceController
    {
        private const string PARTIAL_VIEW_FOLDER = "~/Views/Partials/SiteLayout/";

        /// <summary>
        /// Renders the header partial
        /// </summary>
        /// <returns>Partial view</returns>
        /// 
        public ActionResult RenderHeader()
        {
            return PartialView(PARTIAL_VIEW_FOLDER + "_Header.cshtml");
        }

        /// <summary>
        /// Renders the footer partial
        /// </summary>
        /// <returns>Partial view</returns>
        /// 
        public ActionResult RenderFooter()
        {
            return PartialView(PARTIAL_VIEW_FOLDER + "_Footer.cshtml");
        }

    }
}