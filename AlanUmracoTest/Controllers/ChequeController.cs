﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web;
using Umbraco.Core.Models;
using Umbraco.Web.Mvc;
using AlanUmracoTest.Models;

namespace AlanUmracoTest.Controllers
{
    public class ChequeController : SurfaceController
    {
        private const string PARTIAL_VIEW_FOLDER = "~/Views/Partials/Cheque/";

        /// <summary>
        /// Renders the cheque list partial
        /// </summary>
        /// <returns>Partial view with Cheque model</returns>
        /// 
        public ActionResult RenderChequeList()
        {
            List<ChequeModel> model = new List<ChequeModel>();
            IPublishedContent ChequeListPage = CurrentPage.AncestorOrSelf(1).DescendantsOrSelf().Where(x => x.DocumentTypeAlias == "chequeList").FirstOrDefault();

            foreach (IPublishedContent item in ChequeListPage.Children.OrderByDescending(x => x.GetPropertyValue<string>("chequeDate")))
            {
                string ChequeName = item.Name;

                double ChequeAmount = 0;
                double.TryParse(item.GetPropertyValue<string>("chequeAmount"), out ChequeAmount);

                DateTime chequeDate;
                DateTime.TryParse(item.GetPropertyValue<string>("chequeDate"), out chequeDate);

                string DetailsLink = item.Url;

                model.Add(new ChequeModel(ChequeName, ChequeAmount, chequeDate, DetailsLink));
            }

            return PartialView(PARTIAL_VIEW_FOLDER + "_ChequeList.cshtml", model);
        }

        /// <summary>
        /// Renders the cheque details partial
        /// </summary>
        /// <returns>Partial view</returns>
        /// 
        public ActionResult RenderChequeDetails()
        {
            return PartialView(PARTIAL_VIEW_FOLDER + "_CheckDetails.cshtml");
        }
    }
}