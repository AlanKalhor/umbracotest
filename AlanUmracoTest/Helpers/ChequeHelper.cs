﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace AlanUmracoTest.Helpers
{
    public class ChequeHelper
    {
        private const string AMOUNT_FORMAT = "{0:C}";
        private const string DATE_FORMAT = "dd/MM/yyyy";

        /// Display the amount in currency format
        /// </summary>
        /// <returns>String</returns>
        /// 
        public static string DisplayAmount(string strAmount)
        {
            double amount ;
            double.TryParse(strAmount, out amount);
            return string.Format(AMOUNT_FORMAT, amount);
        }
        /// <summary>
        /// Display the date in dd/MM/yyyy format
        /// </summary>
        /// <returns>String</returns>
        /// 
        public static string DisplayDateShortFormat(string strDate)
        {
            DateTime date;
            DateTime.TryParse(strDate, out date);
            CultureInfo invariant = CultureInfo.InvariantCulture;
            return date.ToString(DATE_FORMAT, invariant);
        }
        /// <summary>
        /// Display the amount of a cheque in words format used in cheque details page
        /// </summary>
        /// <returns>String</returns>
        /// 
        public static string DisplayAmountInWordsFormat(string strAmount)
        {
            double doubleAmount = 0;
            Double.TryParse(strAmount, out doubleAmount);
                
            var beforeFloatingPoint = (int)Math.Floor(doubleAmount);
            var beforeFloatingPointWord = $"{NumberToWords(beforeFloatingPoint)} dollars";
            var afterFloatingPointVal = (int)((doubleAmount - beforeFloatingPoint) * 100);
            var afterFloatingPointWord = (afterFloatingPointVal == 0 ? "" : $" and {SmallNumberToWord(afterFloatingPointVal, "")} cents");
           
            return $"{beforeFloatingPointWord} {afterFloatingPointWord}";
        }

        /// <summary>
        /// Convert a digit to words
        /// </summary>
        /// <returns>String</returns>
        /// 
        private static string NumberToWords(int number)
        {
            if (number == 0)
                return "zero";

            if (number < 0)
                return "minus " + NumberToWords(Math.Abs(number));

            var words = "";

            if (number / 1000000000 > 0)
            {
                words += NumberToWords(number / 1000000000) + " billion ";
                number %= 1000000000;
            }

            if (number / 1000000 > 0)
            {
                words += NumberToWords(number / 1000000) + " million ";
                number %= 1000000;
            }

            if (number / 1000 > 0)
            {
                words += NumberToWords(number / 1000) + " thousand ";
                number %= 1000;
            }

            if (number / 100 > 0)
            {
                words += NumberToWords(number / 100) + " hundred ";
                number %= 100;
            }

            words = SmallNumberToWord(number, words);

            return words;
        }

        /// <summary>
        /// Convert a digit to words used for converting the digits appeared after the floating point a double value
        /// </summary>
        /// <returns>String</returns>

        private static string SmallNumberToWord(int number, string words)
        {
            if (number <= 0) return words;
            if (words != "")
                words += " ";

            var unitsMap = new[] { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
            var tensMap = new[] { "zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

            if (number < 20)
                words += unitsMap[number];
            else
            {
                words += tensMap[number / 10];
                if ((number % 10) > 0)
                    words += "-" + unitsMap[number % 10];
            }
            return words;
        }

    }
}