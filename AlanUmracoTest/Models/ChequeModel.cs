﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AlanUmracoTest.Models
{
    public class ChequeModel
    {
        public string Name { get; set; }
        public double Amount { get; set; }
        public DateTime Date { get; set; }
        public string DetailsLink { get; set; }

        public ChequeModel(string name, double amount, DateTime date, string detailsLink)
        {
            Name = name;
            Amount = amount;
            Date = date;
            DetailsLink = detailsLink;
        }

    }
}